import 'package:flutter/material.dart';

void main() => runApp(HelloFlutterApp());

class HelloFlutterApp extends StatefulWidget{
  @override
  _HelloFlutterAppState createState() => _HelloFlutterAppState();
}

String endlishGreeting = "Hello Flutter";
String spanishGreeting = "Hola Flutter";
String ItalianGreeting = "Ciao Flutter";
class _HelloFlutterAppState extends State<HelloFlutterApp> {
  String displayText = endlishGreeting;
  @override
  Widget build(BuildContext context)
  {
    return MaterialApp(
    debugShowCheckedModeBanner: false,
    home: Scaffold(
    appBar: AppBar(
    title: Text("Hello test 1 2 3 4 test test"),
    leading: Icon(Icons.home),
    actions: <Widget>[
    IconButton(
        onPressed: () {
          setState(() {
            displayText = endlishGreeting;
          });
        },

        icon: Icon(Icons.sports_football)),
      IconButton(
          onPressed: (){
            setState(() {
              displayText = spanishGreeting;
            });
          }, icon: Icon(Icons.sports_soccer)),
      IconButton(
          onPressed: (){
            setState(() {
              displayText = ItalianGreeting;
            });
          }, icon: Icon(Icons.local_pizza))

    ],

    ),
    body: Center(
    child: Text(
    displayText,
    style: TextStyle(fontSize: 24,
    color: Colors.green),
    ),
    ),
    ),
  );
    // return Container();

  }
}

// class HelloFlutterApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       debugShowCheckedModeBanner: false,
//       home: Scaffold(
//         appBar: AppBar(
//           title: Text("Hello test 1 2 3 4 test test"),
//           leading: Icon(Icons.home),
//           actions: <Widget>[
//             IconButton(onPressed: () {}, icon: Icon(Icons.refresh))
//           ],
//
//         ),
//         body: Center(
//           child: Text(
//             "Hello Flutter",
//             style: TextStyle(fontSize: 24,
//             color: Colors.green),
//           ),
//         ),
//       ),
//     );
//   }
// }